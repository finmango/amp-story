# FinMango AMP Story
This is a template used for FinMango AMP stories. To create a new story, all you have to do is:
1. Fork this repository
2. Update the content inside of lesson.json
3. Update the finmango.org domain settings to link to the new URL under finmango.gitlab.io
